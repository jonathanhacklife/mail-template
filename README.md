# Mail Template

Este es un template minimalista hecho por mi para uso personal.
¡Pero usted puede utilizarla si quiere!

Vealo en acción aquí: https://jonathanhacklife.gitlab.io/mail-template

- Solo cambie los valores correspondientes
- Ejecute el proyecto en Chrome, Firefox o su navegador de preferencia
- Dentro del navegador, presione `CTRL+SHIFT+C`
- Pegue el contenido en la configuración de Firma de su mail.

¡Y listo!


En este link se encuentra el material: 
https://www.dropbox.com/sh/ot5gcxcgp82x879/AADGMtd-NCEPxplQwNjmKldna?dl=0

 

/////////////////////////////////////////////////////

 

La firma se compone de:

 

- Tipografía Web Safe: Sans Serif

- Color: #1a2154

- Nombre y Apellido en formato NEGRITA y tamaño 13px

- Detalle sin formato y tamaño 12px

- Barra divisora

- Logotipo de Bank

* Solo en el caso de las firmas comerciales, se agrega una foto de la persona antes de la barra.

 

Se deben subir las imágenes al servidor para obtener una ruta, y luego colocar esa ruta para llamar a la imagen en su lugar correspondiente (barra divisora, logo, foto comercial)

Adjuntamos también un archivo .ai que contiene una foto y una máscara, les sirve de base para colocar fotos reales, exportalas enmascaradas y luego subirlas al servidor.

 

Posicionamiento de objetos:

 

Idealmente, debe haber un margen entre los módulos de 20px.

Y un mínimo de 20px en el margen superior (este puede aumentarse para separar aún más la firma del fin del texto del email)

La barra divisora y la foto tienen un alto de 98px. Sumándole los márgenes confeccionan un mínimo de 138px.

El logo debe ir centrado dentro de esos 98px de altura, para alinearse a los otros 2 componentes.