class MailFormat {
    constructor({ nombre, apellido, puesto, mail, num_tel, direccion, web, avatar, logo }) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.puesto = puesto;
        this.num_tel = num_tel;
        this.mail = mail;
        this.direccion = direccion;
        this.web = web;
        this.logo = logo;
        this.avatar = avatar;
    }
}

function saveAs(uri, filename) {
    var link = document.createElement('a');

    /* Estilizar Link desde una clase */
    link.classList.add("btn", "btn-outline-success")

    if (typeof link.download === 'string') {
        link.href = uri;
        link.download = filename;
        link.innerHTML = "Descargar firma";

        //Firefox requires the link to be in the body
        document.getElementById("firma").appendChild(link)
        //document.body.appendChild(link);

        //simulate click
        //link.click();

        //remove the link when done
        //document.body.removeChild(link);
    } else {
        window.open(uri);
    }
}

function crearMail() {
    /* template = new MailFormat({
        nombre: "Edgardo",
        apellido: "Ivañez",
        puesto: "Ejecutivo comercial",
        mail: "eivanez@banksa.com.ar",
        num_tel: "011 000 0000",
        direccion: "Rivadavia 789, Capital Federal",
        web: "www.banksa.com.ar",
        avatar: "https://media.licdn.com/dms/image/D4D03AQHxRY2an6byEQ/profile-displayphoto-shrink_800_800/0/1671807049944?e=1684368000&v=beta&t=sGD-TOQ_NuAUo9Sj4PI_Qfmi-AAmBNBvonEHK17nDf4",
        logo: "https://i.ibb.co/z2DhfPY/logo.jpg",
    }); */
    let template = new MailFormat(
        {
            nombre: document.getElementById("inputnombre").value,
            apellido: document.getElementById("inputapellido").value,
            puesto: document.getElementById("inputpuesto").value,
            num_tel: document.getElementById("inputnum_tel").value,
            mail: document.getElementById("inputmail").value || document.getElementById("inputnombre").value[0] + document.getElementById("inputapellido").value + `@banksa.com.ar`,
            direccion: document.getElementById("inputdireccion").value || "",
            web: document.getElementById("inputurl").value || "www.banksa.com.ar",
            avatar: document.getElementById("inputavatar").value,
            logo: document.getElementById("inputlogo").value || "https://i.ibb.co/VMzrYgb/Logo-Blue-Bank.png"
        });

    if (template.nombre != "" && template.apellido != "" && template.puesto != "") {

        document.getElementById("firma").innerHTML = `
        <table class="firma" id="tabla" style="
            height: fit-content;
            background-color: #ffffff;
            border-radius: 5px;
            padding: 10px;
            margin: 20px;
            font-family: Open sans, sans-serif">

            <!--NOMBRE Y APELLIDO-->
            <td style="padding: 10px">
                <h3 style="
                    margin: 0;
                    padding: 0;
                    font-size: 13px;
                    font-weight: 600;
                    color: #1a2154;
                    letter-spacing: .5px
                  ">${template.nombre} ${template.apellido}</h3>

                <!--PUESTO-->
                <h4 style="
                    margin: 0;
                    padding-bottom: 10px;
                    font-size: 12px;
                    font-weight: 400;
                    color: #1a2154;
                    text-transform: uppercase;
                    letter-spacing: -.5px">${template.puesto}</h4>

                <!--CONTACTO-->
                <section id="Contacto">
                    <a style="
                    margin: 0;
                    display: block;
                    font-size: 12px;
                    line-height: 1.4;
                    text-decoration: underline;
                    font-weight: 600;
                    color: #1a2154;
                    text-decoration: none;
                    " href="tel:${template.num_tel}">${template.num_tel}</a>

                    <a style="
                    margin: 0;
                    display: block;
                    font-size: 12px;
                    line-height: 1.4;
                    color: #1a2154;
                    text-decoration: underline;
                    text-transform: lowercase;
                    " id="mail" href="mailto:${template.mail}">${template.mail}</a>

                    <p style="
                    margin: 0;
                    font-size: 12px;
                    line-height: 1.4;
                    color: #1a2154;
                    font-weight: bold;
                    " id="dir">${template.direccion}</p>
                    
                    <a style="
                    margin: 0;
                    display: block;
                    font-size: 12px;
                    line-height: 1.4;
                    color: #1a2154;
                    font-weight: bolder;
                    text-decoration: underline;
                    " id="web" href="https://${template.web}" target="_blank">${template.web}</a>

                </section>
            </td>

            <!--LOGOS-->
            <td style="padding: 10px">
                ${template.avatar != "" ? `<img src="${template.avatar}" id="Avatar" style="
                border-radius: 0px 25px 0px 25px;
                width: 98px;
                height: 98px;
                object-fit: contain;" alt="Avatar">` : ""}
                <img src="https://i.ibb.co/HCSYJrm/barra.jpg" id="Divisor" style="margin: 0px 20px" alt="Divisor">
                <img src="${template.logo}" width="118" height="28" style="object-fit: contain;" id="Logo" alt="Logo">
            </td>
        </table>`;
        var resultado = document.getElementById("firma").innerHTML
        document.getElementById("resultado").innerHTML = resultado


        /* CREAR IMAGEN A PARTIR DE UN DIV */

        html2canvas(document.querySelector("#tabla"), {
            imageTimeout: 15000,
            useCORS: true
        }).then(canvas => {
            //document.body.appendChild(canvas)
            saveAs(canvas.toDataURL(), 'Firma.png');
        });
    }
}